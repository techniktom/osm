from math import pow, radians, asinh, tan, pi, log
from typing import List, Tuple
from requests import get
from asyncio import run
from multiprocessing import cpu_count, Process
from os import getpid, environ
from numpy import array, array_split
from time import sleep

PROCESS_COUNT = 250

def deg2xtile(lon_deg: float, zoom: int) -> int:
  return int((lon_deg + 180.0) / 360.0 * pow(2, zoom))

def deg2ytile(lat_deg: float, zoom: int) -> int:
  return int((1.0 - asinh(tan(radians(lat_deg))) / pi) / 2.0 * pow(2, zoom))

def frange(from_num: float, to_num: float, step: float):
    current = from_num

    while True:
        yield current

        current += step

        if current >= to_num:
            break

def partition(from_ytile, to_ytile, from_xtile, to_xtile):
    for ytile in range(from_ytile, to_ytile, -1):
        for xtile in range(from_xtile, to_xtile):
            yield xtile, ytile

def partition2(lst, size):
    for i in range(0, len(lst), size):
        yield lst[i : i+size]

def walk(zoom: int, edge1: Tuple[float, float], edge2: Tuple[float, float]):
    from_xtile = deg2xtile(edge1[1], zoom)
    to_xtile = deg2xtile(edge2[1], zoom)
    from_ytile = deg2ytile(edge1[0], zoom)
    to_ytile = deg2ytile(edge2[0], zoom)
        
    return [list(s) for s in array_split(array(list(partition(from_ytile, to_ytile - 1, from_xtile, to_xtile + 1))), PROCESS_COUNT)]
    
    # if xtile_range == 0:
    #     for x_tile in range(from_xtile, to_xtile+1):
    #         yield partition(from_ytile, to_ytile - 1, x_tile, x_tile+1)
    # else: 
    #     for idx in range(partition_count):
    #         yield partition(from_ytile, to_ytile - 1, from_xtile+idx*xtile_range, from_xtile+(idx+1)*xtile_range)
        
    #     yield partition(from_ytile, to_ytile - 1, from_xtile+partition_count*xtile_range, to_xtile+1)

    

def process_tile(zoom: int, xtile: int, ytile: int, url_template: str):
    url = url_template.format(baseUrl = environ['BASE_URL'], z = zoom, x = xtile, y = ytile)
    try_count = 0
    print('PID:', getpid(), 'URL:', url)
    while True:
        try:
            response = get(url, timeout=500)

            if try_count > 0:
                print('PID:', getpid(), 'URL:', url, 'try:', try_count+1)

            if not response.ok:
                raise Exception(f"Response is not ok ({response.status_code})")

            break
        except Exception as e:
            print(e)
            try_count += 1

            sleep(5*try_count*log(try_count))

            if try_count >= 10:
                break

def process_pertition(tiles: list[Tuple[int, int]], zoom: int, url: str):
    for xtile, ytile in tiles:
        process_tile(zoom, xtile, ytile, url)

    

def main():
    url = '{baseUrl}/api/tile/{z}/{x}/{y}.png'
    edge1 = (50.155429, 12.925536)
    edge2 = (51.052988, 14.851263)

    b = [{        "from_xtile": deg2xtile(edge1[1], zoom),
        "to_xtile" : deg2xtile(edge2[1], zoom),
        "from_ytile" : deg2ytile(edge1[0], zoom),
        "to_ytile" : deg2ytile(edge2[0], zoom),
        } for zoom in range(18)]

    a = [list(walk(zoom, edge1, edge2)) for zoom in range(18)]

    for zoom in range(8, 19):
        print('Zoom: ', zoom)
        processes = []
        
        for partition in walk(zoom, edge1, edge2):
            process = Process(target=process_pertition, args=[partition, zoom, url])
            process.start()
            processes.append(process)
            
        for process in processes:
            process.join()

if __name__ == '__main__':
    main() 
