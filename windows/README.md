# Download data
```{shell}
curl "https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf" -o region.osm.pbf
curl "https://download.geofabrik.de/europe/czech-republic.poly" -o region.poly
```

# Index OSRM server

```{shell}
docker run `
    --rm -t `
    -v osm_osrm-data:/data `
    -v .\region.osm.pbf:/data/region.osm.pbf `
    ghcr.io/project-osrm/osrm-backend `
    osrm-extract -p /opt/car.lua /data/region.osm.pbf
    
docker run --rm -v osm_osrm-data:/data -t ghcr.io/project-osrm/osrm-backend osrm-partition /data/region.osrm
docker run --rm -v osm_osrm-data:/data -t ghcr.io/project-osrm/osrm-backend osrm-customize /data/region.osrm
```

# Index tile server

```{shell}
docker run `
    --rm `
    -v .\region.osm.pbf:/data/region.osm.pbf `
    -v .\region.poly:/data/region.poly `
    -v osm_osmts-data:/data/database/ `
    overv/openstreetmap-tile-server:2.3.0 `
    import
```

# Index nominatim server
```{shell}
docker run `
    -it `
    --rm `
    -e PBF_PATH=/data/region.osm.pbf `
    -e REPLICATION_URL=https://download.geofabrik.de/europe/czech-republic-updates/ `
    -v osm_nominatim-data:/var/lib/postgresql/14/main `
    -v .\region.osm.pbf:/data/region.osm.pbf `
    mediagis/nominatim:4.2
```

```{shell}
docker compose build

docker compose up -d
```