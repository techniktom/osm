package dev.cernohorsky

import com.luciad.imageio.webp.WebPWriteParam
import io.javalin.Javalin
import io.javalin.http.Context
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Duration
import javax.imageio.IIOImage
import javax.imageio.ImageIO
import javax.imageio.ImageWriteParam
import kotlin.math.pow
import kotlin.math.roundToLong

val osmtsBaseUrl: String = System.getenv("OSMTS_BASE_URL")
val port: Int = (System.getenv("PORT") ?: "80").toInt()
val host: String = System.getenv("HOST") ?: "127.0.0.1"

val imagesPath: Path = Paths.get("/data/tiles")
val client: HttpClient = HttpClient.newHttpClient()
const val RETRIES_COUNT = 10

fun getPngImageData(z: String, x: String, y: String): ByteArray {
    return tryGet("http://$osmtsBaseUrl/tile/$z/$x/$y.png")
}

fun tryGet(url: String): ByteArray {
    var retryCount = 0
    while (true) {
        retryCount++

        try {
            val request = HttpRequest
                .newBuilder(URI.create(url))
                .GET()
                .timeout(Duration.ofMillis((100L * 3.0.pow(retryCount)).roundToLong()))
                .build()

            return client
                .send(request, HttpResponse.BodyHandlers.ofByteArray())
                .body()
        } catch (e: IOException) {
            if (retryCount == RETRIES_COUNT) {
                throw RuntimeException(e)
            }
        } catch (e: InterruptedException) {
            if (retryCount == RETRIES_COUNT) {
                throw RuntimeException(e)
            }
        }
    }
}

fun setupContext(ctx: Context, imgType: String, data: ByteArray) {
    ctx
        .result(data)
        .contentType("image/$imgType")
}

fun parseArgument(ctx: Context, pathName: String): String {
    return ctx
        .pathParamAsClass(pathName, String::class.java)
        .check({ it.toIntOrNull() != null }, "Parameter $pathName is not an integer (${ctx.pathParam(pathName)})")
        .get()
}

fun processRequest(ctx: Context, imgType: String, getImageData: (String, String, String) -> ByteArray) {
    val z = parseArgument(ctx, "z")
    val x = parseArgument(ctx, "x")
    val y = parseArgument(ctx, "y")

    val path = imagesPath.resolve("$z-$x-$y.$imgType")

    if (Files.exists(path)) {
        setupContext(ctx, imgType, Files.readAllBytes(path))
        return
    }

    if (!Files.exists(path.parent)) {
        Files.createDirectories(path.parent)
    }

    val imageData = getImageData(z, x, y)

    Files.write(path, imageData)
    setupContext(ctx, imgType, imageData)
}

fun getWebpImageData(z: String, x: String, y: String): ByteArray {
    return try {
        val pngImage = ImageIO.read(ByteArrayInputStream(getPngImageData(z, x, y)))
        val imageDataBuffer = ByteArrayOutputStream()

        val writer = ImageIO.getImageWritersByMIMEType("image/webp").next()

        val writeParam = WebPWriteParam(writer.locale)
        writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT)
        writeParam.setCompressionType(writeParam.getCompressionTypes()[WebPWriteParam.LOSSLESS_COMPRESSION])
        writer.setOutput(imageDataBuffer)

        writer.write(null, IIOImage(pngImage, null, null), writeParam)

        ImageIO.write(pngImage, "webp", imageDataBuffer)
        imageDataBuffer.toByteArray()
    } catch (e: IOException) {
        throw RuntimeException(e)
    }
}

fun main() {
    println("OSMTS_BASE_URL is $osmtsBaseUrl")

    Javalin
        .create()
        .get("/tile/<z>/<x>/<y>.webp") { ctx: Context -> processRequest(ctx, "webp", ::getWebpImageData) }
        .get("/tile/<z>/<x>/<y>.png") { ctx: Context -> processRequest(ctx, "png", ::getPngImageData) }
        .start(host, port)
}

